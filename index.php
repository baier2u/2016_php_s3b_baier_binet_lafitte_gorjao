<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 12/12/2016
 * Time: 17:00
 */
require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use giftbox\controlers\ControlerCatalogue as ControlerCata;
use giftbox\controlers\ControlerCoffret as ControlerCoff;
use giftbox\controlers\ControlerCadeau as ControlerCad;
use giftbox\controlers\ControlerGestionnaire as ControlerGestion;
use giftbox\controlers\ControlerCagnotte as ControlerCagn;
use giftbox\vues\VueCoffret as VueCoff;

session_start();

// en attendant les boutons pour vider le panier, décommentez la ligne ci-dessous pour le faire
//session_destroy();

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

//affiche l'accueil
$app->get('/', function () {
    $catalogue = new ControlerCata();
    $catalogue->menu();
})->name('accueil');

// affiche la liste de toutes les prestations de la base de données
$app->get('/listeprestation', function () {
    $catalogue = new ControlerCata();
    $catalogue->listerPrestations();
})->name('listePrestas');

// affiche la prestation correspondant à l'id passé dans l'url
$app->get('/prestation/:id', function ($id) {
    $catalogue = new ControlerCata();
    $catalogue->donnerPrestation($id);
})->name('prestation');

// affiche la liste de toutes les catégories de la base de données
$app->get('/listecategorie', function () {
    $catalogue = new ControlerCata();
    $catalogue->listerCategories();
})->name('listeCates');

// affiche la liste de toutes les prestations de la catégorie dont l'id est passé dans l'url
$app->get('/catalogue/:id', function ($id) {
    $catalogue = new ControlerCata();
    $catalogue->listerCatalogue($id);
})->name('listeCatalogue');

// affiche le contenu du panier d'un utilisateur
$app->get('/panier', function () {
    $controlcoffret = new ControlerCoff();
    $controlcoffret->listerContenu();
})->name('listeContenuPanier');

// stocke le coffret dans la BDD (pour l'instant ca ne marche pas) vide et affiche le panier vide car il a été validé et pour l'instant le panier n'est pas stocké dans la BDD
$app->post('/panier/valide', function () {
    $controlcoffret = new ControlerCoff();
    $controlcoffret->stockerCoffret();
})->name('listePanierValidé');

$app->post('/panier/cagnotteContri', function () {
    $controlercagnotte = new ControlerCagn();
    $controlercagnotte->demanderMailContributeurs();
})->name('cagnotteContributeur');

$app->post('/panier/cagnotteEnvoyee', function () {
    $controlercagnotte = new ControlerCagn();
    $controlercagnotte->envoyerMailContributeurs();
})->name('envoyerMailContri');

$app->get('/cagnotte', function () {
    $controlercagnotte = new ControlerCagn();
    $controlercagnotte->ouvrirUrlContri();
})->name('ouvrirCagnotte');

$app->get('/cagnotteGestion', function () {
    $controlercagnotte = new ControlerCagn();
    $controlercagnotte->ouvrirUrlGestion();
})->name('ouvrirGestionCagnotte');

$app->get('/cloturerCagnotte', function () {
    $controlercagnotte = new ControlerCagn();
    $controlercagnotte->cloturerCagnotte();
})->name('cloturerCagnotte');

$app->post('/cagnotte/participe', function () {
    $controlercagnotte = new ControlerCagn();
    $controlercagnotte->participerCagno();
})->name('participerCagnotte');

$app->post('/panier/paye', function () {
    $vuec = new VueCoff(null);
    $_SESSION['panier'] = array();
    $_SESSION['nbPrest'] = 0;
    $_SESSION['nbCate'] = 0;
    echo $vuec->render(4);
})->name('listePanierValidé');

// affiche la page où l'utilisateur doit rentrer ses infos pour la validation du panier
$app->get('/panier/infos', function () {
    $vuec = new VueCoff(null);
    echo $vuec->render(5);
})->name('listePanierValidé');

// affiche le contenu du panier car il ne respecte pas les condition pour le valider
$app->get('/panier/nonValide', function () {
    $vuec = new VueCoff($_SESSION['panier']);
    echo $vuec->render(3);
})->name('listePanierNonValidé');

$app->post('/panier/recapitulatif', function () {
    $controlcoffret = new ControlerCoff();
    $vuec = new VueCoff($_SESSION['panier']);
    $controlcoffret->payer();
    echo $vuec->render(9);
})->name('recapitulatif');

// affiche une page qui génère une url cadeau qui sera envoyé à un destinataire
$app->get('/cadeau', function () {
    $controlcadeau = new ControlerCad();
    $controlcadeau->genererURLCadeau();
})->name('URLCadeau');

// affiche une page qui affiche le contenu du cadeau envoyé
$app->get('/offrecadeau', function () {
    $controlcadeau = new ControlerCad();
    $controlcadeau->listerContenuCadeau();
})->name('offreCadeau');

// affiche une page qui dit que le mail a bien été envoyé au destinataire
$app->post('/cadeau/envoye', function () {
    $controlcadeau = new ControlerCad();
    $controlcadeau->envoyerMail();
})->name('cadeauEnvoye');


if(isset($_SESSION['urlGestion'])){
$app->get('/gestionCadeau/'.$_SESSION['urlGestion'], function () {
    $controlCadeau = new ControlerCad();
    $controlCadeau->gererCadeau();
})->name('gestionCadeau');
}

if(isset($_SESSION['urlGestion'])){
$app->post('/gestionCadeau/'.$_SESSION['urlGestion'], function () {
    $controlCadeau = new ControlerCad();
    $controlCadeau->gererCadeau();
})->name('gestionCadeau');
}

$app->get('/inscription', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->listerFormulaireInscription();
})->name('inscription');

$app->post('/validation', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->verifierInscription();
})->name('verifierInscription');

$app->get('/connexion', function () {
    if (isset($_SESSION['connexion'])) {
        $vuegestion = new \giftbox\vues\VueGestionnaire(null);
        echo $vuegestion->render(4);
    } else {
        $vuegestion = new \giftbox\vues\VueGestionnaire(null);
        echo $vuegestion->render(3);
    }
})->name('afficherPageConnexion');

$app->post('/connexion', function () {
    if (isset($_SESSION['connexion'])) {
        $vuegestion = new \giftbox\vues\VueGestionnaire(null);
        echo $vuegestion->render(4);
    } else {
        $vuegestion = new \giftbox\vues\VueGestionnaire(null);
        echo $vuegestion->render(3);
    }
})->name('afficherPageConnexion');

$app->get('/deconnexion', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->deconnecter();
})->name('afficherPageDeconnexion');

$app->post('/gestionnaire', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->verifierConnexion();
})->name('verifierInscription');

$app->get('/ajoutPrestation', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->ajouterPrestation();
})->name('suppressionPrestation');

$app->post('/ajoutPrestation', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->ajouterPrestation();
})->name('suppressionPrestation');

$app->get('/suppressionPrestation', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->supprimerPrestation();
})->name('suppressionPrestation');

$app->get('/desactivationPrestation', function () {
    $controlGestion = new ControlerGestion();
    $controlGestion->desactiverPrestation();
})->name('desactivationPrestation');

$app->run();