<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 03/01/2017
 * Time: 17:14
 */

namespace giftbox\controlers;

use giftbox\models\Gestionnaire as Gestion;
use giftbox\models\Prestation as Prest;
use giftbox\vues\VueGestionnaire as VueGestion;

class ControlerGestionnaire
{

    private $app;

    function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
    }

    function listerFormulaireInscription()
    {
        $vueUtil = new VueGestion(null);
        echo $vueUtil->render(1);
    }

    function verifierInscription()
    {
        $postIdentifiant = $this->app->request->post('identifiant');
        $postPasswd = $this->app->request->post('passwd');

        $postIdentifiant = filter_var($postIdentifiant, FILTER_SANITIZE_STRING);
        $postPasswd = filter_var($postPasswd, FILTER_SANITIZE_STRING);

        $hash = password_hash($postPasswd, PASSWORD_DEFAULT, ['cost' => 12]);

        if (isset($postIdentifiant)) {
            $ges = new Gestion();
            $ges->identifiant = $postIdentifiant;
            $ges->mdp = $hash;
            $ges->save();
            $vuegestion = new VueGestion(null);
            echo $vuegestion->render(2);
        }
    }

    function verifierConnexion()
    {
        $postIdentifiant = $this->app->request->post('identifiant');
        $postPasswd = $this->app->request->post('passwd');

        $postIdentifiant = filter_var($postIdentifiant, FILTER_SANITIZE_STRING);
        $postPasswd = filter_var($postPasswd, FILTER_SANITIZE_STRING);

        if (isset($postIdentifiant)) {
            $gestion = Gestion::where('identifiant', '=', $postIdentifiant)
                ->first();
            if (isset($gestion)) {
                if (password_verify($postPasswd, $gestion['mdp'])) {
                    $vuegestion = new VueGestion(null);
                    echo $vuegestion->render(4);
                    $_SESSION['connexion'] = $gestion;
                } else {
                    $vuegestion = new VueGestion(null);
                    echo $vuegestion->render(5);
                }
            } else {
                $vuegestion = new VueGestion(null);
                echo $vuegestion->render(5);
            }
        }
    }

    function supprimerPrestation()
    {
        if (isset($_SESSION['connexion'])) {
            $idPrestation = $this->app->request->get('sup');
            if (isset($idPrestation)) {
                $idPrestation = filter_var($idPrestation, FILTER_SANITIZE_NUMBER_INT);
                $prestation = Prest::where('id', '=', $idPrestation)
                    ->first();
                if (isset($prestation)) {
                    if (isset($_COOKIE[$prestation['nom']]))
                        unset($_COOKIE[$prestation['nom']]);
                    $prestation->delete();
                    $vuegestion = new VueGestion("La prestation " . $idPrestation . " a bien été supprimée définitivement.");
                    echo $vuegestion->render(7);
                } else {
                    $vuegestion = new VueGestion("La prestation " . $idPrestation . " n'existe pas.");
                    echo $vuegestion->render(7);
                }
            } else {
                $vuegestion = new VueGestion(null);
                echo $vuegestion->render(6);
            }
        } else {
            $vuegestion = new VueGestion(null);
            echo $vuegestion->render(3);
        }
    }

    function ajouterPrestation()
    {
        if (isset($_SESSION['connexion'])) {
            $nomPrestation = $this->app->request->post('nom');
            $descrPrestation = $this->app->request->post('descr');
            $idCate = $this->app->request->post('cat_id');
            $prix = $this->app->request->post('prix');
            if (isset($nomPrestation)) {
                $image = $_FILES['img'];
                $nomPrestation = filter_var($nomPrestation, FILTER_SANITIZE_STRING);
                $descrPrestation = filter_var($descrPrestation, FILTER_SANITIZE_STRING);
                $idCate = filter_var($idCate, FILTER_SANITIZE_NUMBER_INT);
                $prix = filter_var($prix, FILTER_SANITIZE_NUMBER_INT);
                $filename = $image['name'];

                $prestation = Prest::where('nom', '=', $nomPrestation)
                    ->first();
                if (!isset($prestation))
                    $prestation = new Prest();

                $prestation->nom = $nomPrestation;
                $prestation->descr = $descrPrestation;
                $prestation->cat_id = $idCate;
                $prestation->img = $filename;
                $prestation->prix = $prix;
                $prestation->save();

                $file = $image['tmp_name'];
                rename($file, "web/img/$filename");

                $vuegestion = new VueGestion("La prestation " . $nomPrestation . " a bien été ajoutée.");
                echo $vuegestion->render(9);
            } else {
                $vuegestion = new VueGestion(null);
                echo $vuegestion->render(8);
            }
        } else {
            $vuegestion = new VueGestion(null);
            echo $vuegestion->render(3);
        }
    }

    function desactiverPrestation()
    {
        if (isset($_SESSION['connexion'])) {
            $idPrestation = $this->app->request->get('des');
            $tempsDes = $this->app->request->get('time');
            if (isset($idPrestation)) {
                $idPrestation = filter_var($idPrestation, FILTER_SANITIZE_NUMBER_INT);
                $prestation = Prest::where('id', '=', $idPrestation)
                    ->first();
                if (isset($prestation)) {
                    $tempsDes = filter_var($tempsDes, FILTER_SANITIZE_NUMBER_INT);
                    setcookie($prestation['nom'], 'désactivée', (time() + ($tempsDes * 24 * 3600)));
                    $vuegestion = new VueGestion("La prestation " . $idPrestation . " a bien été désactivé pendant " . $tempsDes . " jours.");
                    echo $vuegestion->render(11);
                } else {
                    $vuegestion = new VueGestion("La prestation " . $idPrestation . " n'existe pas ou est invalide.");
                    echo $vuegestion->render(11);
                }
            } else {
                $vuegestion = new VueGestion(null);
                echo $vuegestion->render(10);
            }
        } else {
            $vuegestion = new VueGestion(null);
            echo $vuegestion->render(3);
        }
    }

    function deconnecter()
    {
        unset($_SESSION['connexion']);
        $vuegestion = new VueGestion("Vous avez bien été déconnecté.");
        echo $vuegestion->render(12);
    }
}