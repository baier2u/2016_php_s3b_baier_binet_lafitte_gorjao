<?php

/**
 * Created by PhpStorm.
 * User: baier
 * Date: 12/12/2016
 * Time: 17:04
 */
namespace giftbox\controlers;


use giftbox\models\Prestation as Prest;
use giftbox\models\Categorie as Cate;
use giftbox\vues\VueCatalogue as VueCata;

class ControlerCatalogue
{
    private $app;

    function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
    }

    function menu(){
        $vuec = new VueCata(null);
        echo($vuec->render(5));
    }

    function listerPrestations()
    {
        $pres = null;
        $get = $this->app->request->get('triprix');
        if (isset($get)) {
            $get = filter_var($get, FILTER_SANITIZE_STRING);
            if ($get == 'c')
                $pres = Prest::orderBy('prix')->get();
            else if ($get == 'd')
                $pres = Prest::orderBy('prix', 'desc')->get();
        } else {
            $pres = Prest::get();
        }
        $vuec = new VueCata($pres);
        echo($vuec->render(1));
    }

    function donnerPrestation($id)
    {
        $pres = Prest::where('id', '=', $id)->first();
        $vuec = new VueCata($pres);
        echo($vuec->render(2));
    }

    function listerCategories()
    {
        $cate = Cate::get();
        $vuec = new VueCata($cate);
        echo($vuec->render(4));
    }

    function listerCatalogue($id)
    {
        $pres = null;
        $get = $this->app->request->get('triprix');
        if (isset($get)) {
            $get = filter_var($get, FILTER_SANITIZE_STRING);
            if ($get == 'c')
                $pres = Prest::where('cat_id', '=', $id)->orderBy('prix')->get();
            else if ($get == 'd')
                $pres = Prest::where('cat_id', '=', $id)->orderBy('prix', 'desc')->get();
        } else
            $pres = Prest::where('cat_id', '=', $id)->get();
        $vuec = new VueCata($pres);
        echo($vuec->render(3));
    }
}