<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 19/12/2016
 * Time: 17:56
 */

namespace giftbox\controlers;

use giftbox\models\Coffret as Coffret;
use giftbox\models\Cagnotte as Cagnotte;
use giftbox\models\Contribuer as Contribu;
use giftbox\vues\VueCoffret as VueCoff;
use giftbox\models\Prestation as Prest;

class ControlerCoffret
{
    private $app;

    function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
    }

    function listerContenu()
    {
        // récupère l'id de la prestation passé en paramètre de l'url (ex: ../panier?idprest=11) pour pouvoir l'ajouter au panier
        $get = $this->app->request->get('idprest');

        // récupère l'id de la prestation passé en paramètre de l'url (ex: ../panier.suppression=11) pour pouvoir la supprimer du panier
        $post = $this->app->request->get('sup');

        // récupère le paramètre de l'url (vider) pour vider le panier
        $vider = $this->app->request->get('vider');

        // si $get a une valeur, ajoute la prestation correspondante au panier
        if (isset($get)) {
            $get = filter_var($get, FILTER_SANITIZE_NUMBER_INT);
            $this->ajouter($get);
        }
        // si $post a une valeur, supprime la prestation correspindante du panier
        if (isset($post)) {
            $post = filter_var($post, FILTER_SANITIZE_NUMBER_INT);
            if (filter_var($post, FILTER_VALIDATE_INT))
                $this->supprimer($post);
            else
                echo("<div id='sous'>Id de prestation invalide</div>");
        }
        // si $vider a une valeur, supprime toutes les prestations du panier et remet le nombre de prestations pour valider le panier a 0
        if (isset($vider)) {
            $_SESSION['panier'] = array();
            $_SESSION['nbPrest'] = 0;
            $_SESSION['nbCate'] = 0;
        }

        if (isset($_SESSION['panier']) && sizeof($_SESSION['panier']) > 0) {
            $vuec = new VueCoff($_SESSION['panier']);
            echo($vuec->render(1));
        } else {
            // l'attribut du controler sera null juste pour qu'on puisse afficher que le panier est vide
            $vuec = new VueCoff(null);
            echo($vuec->render(2));
        }
    }

    function ajouter($id)
    {
        $ajoute = Prest::where('id', '=', $id)->first();
        if (isset($_SESSION['panier'])) {
            $existePrest = false;
            $existeCate = false;
            foreach ($_SESSION['panier'] as $p) {
                // teste si la prestation qui a été ajouté est déjà présente ou si sa catégorie est déjà présente
                // pour savoir si on incrémente $_SESSION['nb'] ou non
                if ($ajoute['id'] == $p['id']) {
                    $existePrest = true;
                }
                if ($ajoute['cat_id'] == $p['cat_id']) {
                    $existeCate = true;
                }
            }

            if (!$existePrest) {
                $_SESSION['panier'][] = $ajoute;
                // on incrémente car la prestation ajoutée n'était pas présente avant et sa catégorie non plus
                $_SESSION['nbPrest'] += 1;
                if (!$existeCate)
                    $_SESSION['nbCate'] += 1;
            }
        } else {
            // initialisation des sessions
            $_SESSION['panier'] = array();
            $_SESSION['panier'][] = $ajoute;
            $_SESSION['nbPrest'] = 1;
            $_SESSION['nbCate'] = 1;
        }
    }

    function supprimer($id)
    {
        // crée un tableau qui va contenir toutes les prestations dont l'id est différent de celui que l'on veut supprimer
        // ensuite la variable $_SESSION['panier'] prend comme valeur le tableau créé
        $pres = Prest::where('id', '=', $id)->first();
        $tab = array();
        foreach ($_SESSION['panier'] as $p) {
            if ($p['id'] != $id)
                $tab[] = $p;
        }
        $_SESSION['panier'] = $tab;
        $_SESSION['nbPrest'] -= 1;
        $existeCate = false;
        foreach ($_SESSION['panier'] as $p) {
            // idem que quand on ajoute
            if ($pres['cat_id'] == $p['cat_id']) {
                $existeCate = true;
                break;
            }
        }
        if (!$existeCate && isset($pres)) {
            // on décrémente si la prestation qu'on a supprimé n'existe plus et idem pour sa catégorie
            // mais on décrémente aussi seulement si on a réellement supprimé une prestation
            $_SESSION['nbCate'] -= 1;
        }
    }

    // méthode qui va srocker le coffret dans la BDD
    public function stockerCoffret()
    {
        $nom = $this->app->request->post('nom');
        $prenom = $this->app->request->post('prenom');
        $mail = $this->app->request->post('mailUtil');
        $message = $this->app->request->post('message');
        $paiement = $this->app->request->post('paiement');

        $_SESSION['mailUtil'] = $mail;

        $urlGestion = bin2hex(openssl_random_pseudo_bytes(32));
        $urlCadeau = bin2hex(openssl_random_pseudo_bytes(32));

        $_SESSION['urlGestion'] = $urlGestion;
        $_SESSION['url'] = $urlCadeau;

        $tabInt = array();
        foreach ($_SESSION['panier'] as $p) {
            $tabInt[] = $p['id'];
        }
        $contenu = serialize($tabInt);

        // si s'il y a une valeur dans post('nom') on peut stocker le coffret dans la BDD
        // mais ici ca ne marche te pourtant j'ai rentré des valeurs dans les cases dont le nom
        $c = new Coffret();
        if (isset($nom)) {
            $nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $prenom = filter_var($prenom, FILTER_SANITIZE_STRING);
            $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
            $message = filter_var($message, FILTER_SANITIZE_STRING);
            $paiement = filter_var($paiement, FILTER_SANITIZE_STRING);

            $c->nom = $nom;
            $c->prenom = $prenom;
            $c->mail = $mail;
            $c->message = $message;
            $c->montant = $_SESSION['montantPanier'];
            $c->paiement = $paiement;
            $c->etat = "non payé";
            $c->contenu = $contenu;
            $c->urlGestion = $_SESSION['urlGestion'];
            $c->urlCadeau = $_SESSION['url'];
            $c->save();
        }

        $vuec = new VueCoff(null);
        if ($paiement == "Cagnotte") {
            $urlGestionCagnotte = bin2hex(openssl_random_pseudo_bytes(32));
            $urlContri = bin2hex(openssl_random_pseudo_bytes(32));
            $_SESSION['urlGestionCagnotte'] = $urlGestionCagnotte;

            $cagnotte = new Cagnotte();
            $cagnotte->montant = 0;
            $cagnotte->urlGestionCagn = $urlGestionCagnotte;
            $cagnotte->urlContri = $urlContri;
            $cagnotte->save();
            $_SESSION['idCagnotte'] = $cagnotte['id'];

            $contribution = new Contribu();
            $contribution->id_coffret = $c['id'];
            $contribution->id_cagnotte = $cagnotte['id'];
            $contribution->montant_contri = 0;
            $contribution->save();

            echo($vuec->render(8));
        } else {
            echo($vuec->render(6));
        }
    }

    public function payer()
    {
        $url = $_SESSION['url'];
        $c = Coffret::where('urlCadeau', '=', $url)
            ->first();
        $c->etat = "payé";
        $c->save();
        $_SESSION['cadeauEnvoye'] = false;
    }
}