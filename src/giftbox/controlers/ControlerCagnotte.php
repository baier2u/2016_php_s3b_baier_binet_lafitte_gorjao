<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 12/01/2017
 * Time: 21:58
 */

namespace giftbox\controlers;

use giftbox\models\Prestation as Prest;
use giftbox\models\Cagnotte as Cagn;
use giftbox\models\Coffret as Coffret;
use giftbox\models\Contribuer as Contribu;
use giftbox\vues\VueCagnotte as VueCagn;
use giftbox\controlers\ControlerCoffret as ControlerCoff;

class ControlerCagnotte
{
    private $app;

    function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
    }

    public function demanderMailContributeurs()
    {
        $nbPers = $this->app->request->post('nbpers');
        $nbPers = filter_var($nbPers, FILTER_SANITIZE_NUMBER_INT);
        $_SESSION['nbContri'] = $nbPers;
        $vuecag = new VueCagn($nbPers);
        echo($vuecag->render(1));
    }

    public function envoyerMailContributeurs()
    {
        $cagnotte = Cagn::where('id', '=', $_SESSION['idCagnotte'])
            ->first();
        for ($i = 0; $i < $_SESSION['nbContri']; $i++) {
            $param = "mailContri" . $i;
            $mail = $this->app->request->post($param);
            $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
            $urlCagnotte = "https://webetu.iutnc.univ-lorraine.fr/www/gorjao1u/2016_php_s3b_baier_binet_lafitte_gorjao/cagnotte?u=" . $cagnotte->urlContri;
            $_SESSION['urlCagnotte'] = $urlCagnotte;
            mail("$mail", "Cagnotte", "Bonjour !\n\nJe compte offrir un cadeau à un proche mais j'ai besoin de contributeurs "
            . "pour remplir ma cagnotte et pouvoir acheter le coffret cadeau.\n"
            . "Merci de bien vouloir m'aider en contribuant à ma cagnotte.\n\n"
            . "Bonne journée.\n\nA bientôt.\n\n"
            . "$urlCagnotte", "From: " . $_SESSION['mailUtil'] . "\r\n", "Reply-To: " . $_SESSION['mailUtil'] . "\r\n");
        }
        $vuecag = new VueCagn(null);
        echo($vuecag->render(2));
    }

    public function ouvrirUrlContri()
    {
        $urlCagnotte = $this->app->request->get('u');
        $part = $this->app->request->get('p');

        if (isset($urlCagnotte)) {
            $_SESSION['urlCagnotte'] = $urlCagnotte;
            $cagnotte = Cagn::where('urlContri', '=', $urlCagnotte)
                ->first();
            if (isset($cagnotte)) {
                $contribution = Contribu::where('id_cagnotte', '=', $cagnotte['id'])
                    ->first();
                $coffret = Coffret::where('id', '=', $contribution['id_coffret'])
                    ->first();

                $_SESSION['montantContri'] = $contribution['montant_contri'];

                $contenuId = unserialize($coffret['contenu']);
                $contenuCadeau = array();
                foreach ($contenuId as $id) {
                    $contenuCadeau[] = Prest::where('id', '=', $id)
                        ->first();
                }
                $vuecag = new VueCagn($contenuCadeau);
                echo($vuecag->render(3));
            } else {
                $vuecag = new VueCagn(null);
                echo($vuecag->render(9));
            }
        } else if (isset($part)) {
            $vuecag = new VueCagn(null);
            echo($vuecag->render(4));
        }
    }

    public function participerCagno()
    {
        $montant = $this->app->request->post('montant');
        $montant = filter_var($montant, FILTER_SANITIZE_NUMBER_INT);
        $cagnotte = Cagn::where('urlContri', '=', $_SESSION['urlCagnotte'])
            ->first();
        $contribution = Contribu::where('id_cagnotte', '=', $cagnotte['id'])
            ->first();
        $contribution->montant_contri += $montant;
        $_SESSION['montantContri'] += $montant;
        $contribution->save();

        $vuecag = new VueCagn(null);
        echo($vuecag->render(5));
    }

    public function ouvrirUrlGestion()
    {
        $urlGestionCagnotte = $this->app->request->get('u');

        if (isset($urlGestionCagnotte)) {
            $vuecag = new VueCagn(null);
            echo($vuecag->render(6));
        }
    }

    public function cloturerCagnotte()
    {
        $urlGestionCagnotte = $this->app->request->get('u');
        $cagnotte = Cagn::where('urlGestionCagn', '=', $urlGestionCagnotte)
            ->first();

        $valide = $this->app->request->get('ca');
        if (isset($valide)) {
            if ($valide == 'valide') {
                $contribution = Contribu::where('id_cagnotte', '=', $cagnotte['id'])
                    ->first();
                $contribution->delete();
                $cagnotte->delete();

                $controlCoff = new ControlerCoff();
                $controlCoff->payer();

                $vuecag = new VueCagn(null);
                echo($vuecag->render(8));
            } else {
                $vuecag = new VueCagn(null);
                echo($vuecag->render(7));
            }
        }
    }
}