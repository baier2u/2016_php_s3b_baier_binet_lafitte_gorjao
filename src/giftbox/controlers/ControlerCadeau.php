<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 29/12/2016
 * Time: 15:37
 */

namespace giftbox\controlers;

use giftbox\models\Prestation as Prest;
use giftbox\vues\VueCadeau as VueCad;
use giftbox\models\Coffret as Coff;

class ControlerCadeau
{
    private $app;

    function __construct()
    {
        $this->app = \Slim\Slim::getInstance();
    }

    function genererURLCadeau()
    {
        $urlCadeau = "https://webetu.iutnc.univ-lorraine.fr/www/gorjao1u/2016_php_s3b_baier_binet_lafitte_gorjao/offrecadeau?u=" . $_SESSION['url'];
        $_SESSION['urlCadeau'] = $urlCadeau;
        $coffret = Coff::where('urlCadeau', '=', $_SESSION['url'])
            ->first();
        $vuecad = new VueCad($coffret);
        echo($vuecad->render(1));
    }

    function listerContenuCadeau()
    {
        $getu = $this->app->request->get('u');
        $getAff = $this->app->request->get('a');
        $coffret = null;
        $contenu = array();

        if (isset($getu)) {
            $getu = filter_var($getu, FILTER_SANITIZE_STRING);
            if (isset($getAff)) {
                $getAff = filter_var($getAff, FILTER_SANITIZE_STRING);
                $getUrl = $getu . "&a=" . $getAff;

                $coffret = Coff::where('urlCadeau', '=', $getUrl)
                    ->first();
                $contenuCadeau = unserialize($coffret['contenu']);
                foreach ($contenuCadeau as $id) {
                    $contenu[] = Prest::where('id', '=', $id)
                        ->first();
                }
            }
        }

        if ($getAff == "i") {
            $getPage = $this->app->request->get('page');
            $getPage = filter_var($getPage, FILTER_SANITIZE_STRING);
            $_SESSION['contenuCadeau'] = $contenu;

            if (isset($_SESSION['iteration'])) {
                if ($getPage == 'suiv') {
                    $_SESSION['iteration'] += 1;
                    $vuecad = new VueCad($contenu[$_SESSION['iteration']]);
                    echo($vuecad->render(3));
                } else if ($getPage == 'prec') {
                    $_SESSION['iteration'] -= 1;
                    $vuecad = new VueCad($contenu[$_SESSION['iteration']]);
                    echo($vuecad->render(3));
                } else {
                    $_SESSION['iteration'] = 0;
                    $vuecad = new VueCad($contenu[$_SESSION['iteration']]);
                    echo($vuecad->render(3));
                }
                if ($_SESSION['iteration'] == sizeof($contenu)-1) {
                    $coffret->etatCadeau = "entièrement ouvert";
                    $coffret->save();
                }
                else if ($coffret['etatCadeau'] != "entièrement ouvert") {
                    $coffret->etatCadeau = "en cours d'ouverture par le destinataire";
                    $coffret->save();
                }
            }
        } else {
            $dateSys = strftime("%Y-%m-%d", time());
            $dateCadeau = $coffret['dateCadeau'];
            $timestampSys = strtotime($dateSys);
            $timestampCad = strtotime($dateCadeau);

            $vuecad = new VueCad($contenu);
            if ($timestampSys >= $timestampCad) {
                $coffret->etatCadeau = "entièrement ouvert";
                $coffret->save();
                echo($vuecad->render(2));
            } else {
                echo($vuecad->render(5));
            }
        }
    }

    function envoyerMail()
    {
        // pour envoyer un mail il faut utiliser la fonction mail
        $url = $_SESSION['urlCadeau'];
        $coffret = Coff::where('urlCadeau', '=', $_SESSION['url'])
            ->first();
        $message = $coffret['message'];

        $postMail = $this->app->request->post('mailDestinataire');
        $postMail = filter_var($postMail, FILTER_SANITIZE_EMAIL);

        $dateCadeau = $this->app->request->post('dateCadeau');
        $postPasswd = $this->app->request->post('mdp');

        $postPasswd = filter_var($postPasswd, FILTER_SANITIZE_STRING);

        $hash = password_hash($postPasswd, PASSWORD_DEFAULT, ['cost' => 12]);

        $coffret->mdpGestion = $hash;
        $coffret->dateCadeau = $dateCadeau;
        $coffret->save();

        $parametreUrl = null;
        $affichage = $this->app->request->post('affichage');
        $affichage = filter_var($affichage, FILTER_SANITIZE_STRING);

        if ($affichage == "Individuel") {
            $parametreUrl = 'i';
            $_SESSION['iteration'] = 0;
        } else
            $parametreUrl = 't';

        if (isset($_SESSION['cadeauEnvoye'])) {
            if (!$_SESSION['cadeauEnvoye']) {
                $url .= "&a=" . $parametreUrl;
                $_SESSION['url'] .= "&a=" . $parametreUrl;
            }
        }

        $coffret->urlCadeau = $_SESSION['url'];
        $coffret->etatCadeau = "transmis au destinataire";
        $coffret->save();

        mail("$postMail", "Cadeaux", "$message.\n$url", "From: " . $_SESSION['mailUtil'], "Reply-To: " . $_SESSION['mailUtil']);
        $_SESSION['cadeauEnvoye'] = true;

        $vuecad = new VueCad(null);
        echo($vuecad->render(4));
    }

    function gererCadeau()
    {
        $mdp = $this->app->request->post('mdp');
        if (isset($mdp)) {
            $urlGestion = filter_var($_SESSION['urlGestion'], FILTER_SANITIZE_URL);
            $coffret = Coff::where('urlGestion', '=', $urlGestion)
                ->first();
            unset($mdp);
            $vuecad = new VueCad($coffret);
            echo($vuecad->render(6));
        } else {
            $vuecad = new VueCad(null);
            echo($vuecad->render(7));
        }
    }
}