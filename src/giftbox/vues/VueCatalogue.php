<?php

/**
 * Created by PhpStorm.
 * User: baier
 * Date: 15/12/2016
 * Time: 08:44
 */
namespace giftbox\vues;

use giftbox\models\Categorie as Cate;

class VueCatalogue
{
    public $content, $objet, $image, $app;

    function __construct($tab)
    {
        $this->objet = $tab;
        $this->app = \Slim\Slim::getInstance()->request->getRootUri();
    }

    public function render($num)
    {
        $res = null;
        switch ($num) {
            case 1 : {
                foreach ($this->objet as $p) {
                    $this->content .= "<div id='sous'><H2>" . $p['id'] . "</H2>  <H1>" . $p['nom'] . "<img src=\"$this->app/web/img/" . $p['img'] . "\" alt=\"" . $p['img'] . "\">"  . $p['descr'] ."<br><br>". $p['prix'] . " €</H1><br>";

                    $this->content .= "<a href=\"prestation/" . $p['id'] . "\"> description </a><br></div><br>";
                }
                $res = $this->afficherListePrestation();
                break;
            }
            case 2 : {
                $cate = Cate::where('id', '=', $this->objet['cat_id'])->first();
                $this->image = $this->objet['img'];
                $this->content = "<div><H2>" . $this->objet['id'] . "</H2> <H1>" . $this->objet['nom'] . " </H1></br><H1>" . $this->objet['descr'] . " </H1></br><H1> " . $cate['nom'] . "</H1></div>";
                $res = $this->afficherPrestation($this->objet['nom'], $this->objet['id']);
                break;
            }
            case 3 : {
                $cate = Cate::where('id', '=', $this->objet[0]['cat_id'])->first();
                foreach ($this->objet as $p) {
                    $this->content .= "<div id='sous'><H2>" . $p['id'] . "</H2>  " . " <H1>" . $p['nom'] . "</H1>" . "<img src=\"$this->app/web/img/" . $p['img'] . "\" alt=\"" . $p['img'] . "\">" . "<br><H1>" . $p['descr'] . "</H1></br><H1>" . $p['prix'] . " €</H1><br>";
                    $this->content .= "<a href=\"../prestation/" . $p['id'] . "\"> description </a><br></div><br>";
                }
                $res = $this->afficherCatalogue($cate['nom'], $cate['id']);
                break;
            }
            case 4 : {
                $this->content = "<div>";
                foreach ($this->objet as $c) {
                    $href = "./catalogue/" . $c['id'];
                    $this->content .= "<div id='sous'><a href='$href'><H2>" . $c['id'] . "</H2>  <H1>" . $c['nom'] . "</H1></a></div><br>";
                }
                $this->content .= "</div>";
                $res = $this->afficherListeCategorie();
                break;
            }
            case 5 : {
                $res = $this->menu();
                $res .= $this->accueil();
                break;
            }
        }
        return $res;
    }

    private function afficherListePrestation()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            <div class="tri">
            <a href="./listeprestation?triprix=c"> Trier par prix croissant </a><br>
            <a href="./listeprestation?triprix=d"> Trier par prix décroissant </a><br>
            $this->content
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPrestation($nom, $id)
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head> 
            <title>$nom</title> 
            </head>
            <body>
            <div id='sous'>
            $this->content
            <img src="$this->app/web/img/$this->image" alt="$this->image">
            <a href="../panier?idprest=$id">
                <input type="submit" value="Ajouter au panier">
            </a> 
            <form action="../listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>            
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherCatalogue($nom, $id)
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>  
            <title>$nom</title> </head>
            <body>
            <div class='tri'>
            <a href="./$id?triprix=c"> Trier par prix croissant </a><br>
            <a href="./$id?triprix=d"> Trier par prix décroissant </a><br>
            $this->content
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherListeCategorie()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head> 
            <title>Categories</title></head>
            <body>
            $this->content
            </body>
            </html>
END;
        return $html;
    }

//genere l'entete du site à inclure sur l'html de toutes les pages!!!!!!!!!
    public function menu()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="./web/CSS/bootstrap.css" rel="stylesheet">
    <link href="./web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="./">Accueil</a></li>
                <li><a href="./panier">Panier</a></li>
                <li><a href="./listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="./listecategorie">Categories</a></li>
                <li>  <a href="./connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }

    // même menu que celui au-dessus mais adapté pour qu'il fonctionne avec la liste des prestations d'une catégorie
    public function menu2()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="../web/CSS/bootstrap.css" rel="stylesheet">
    <link href="../web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="../">Accueil</a></li>
                <li><a href="../panier">Panier</a></li>
                <li><a href="../listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="../listecategorie">Categories</a></li>
                <li>  <a href="../connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }

    public function accueil()
    {
        $html = <<<END
            <!DOCTYPE html>
                <html>
                <body>
                    <div>
                        <p>
                        
                            <br>Bienvenue sur le site de GIFTBOX !!!
                            </br>Ici vous pouvez choisir plusieurs activités parmi de nombreuses catégories.
                            </br>Pour cela inscrivez-vous et ensuite choissisez ce que vous voulez acheter!
                            </br>Vous pouvez payer en Carte Bleu ou bien avec des bons.
                            </br>Toute l'équipe espère que vous allez passer un bon moment sur notre site.
                            </br>Bisous
                        </p>
                    </div>
                </body>
                </html>
END;
        return $html;
    }
}