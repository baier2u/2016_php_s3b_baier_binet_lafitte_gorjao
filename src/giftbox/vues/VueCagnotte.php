<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 12/01/2017
 * Time: 22:03
 */

namespace giftbox\vues;


class VueCagnotte
{
    public $objet, $content, $appRoot;

    function __construct($tab)
    {
        $this->objet = $tab;
        $this->appRoot = \Slim\Slim::getInstance()->request->getRootUri();
    }

    public function render($num)
    {
        $res = null;
        switch ($num) {
            case 1 : {
                $this->content = "<div id='sous'><h1><a><p><b><label for=\"mail\">Entrez l'adresse mail de tous les contributeurs :</label>";
                for ($i = 0; $i < $this->objet; $i++) {
                    $this->content .= "<input type=\"email\" id=\"mail\" name=\"mailContri$i\"
                maxlength=\"40\" placeholder=\"Ex : dupontjean@gmail.com\" required><br>";
                }
                $this->content .= "</b></a></h1></p>";
                $res = $this->afficherDemandeMail();
                break;
            }
            case 2 : {
                $this->content = "<div id='sous'><H1>L'url pour contribuer à votre cagnotte a bien été envoyée aux contributeurs: <br>" . $_SESSION['urlCagnotte'] . "</H1>";
                $res = $this->afficherUrlCagnotteEnvoye();
                break;
            }
            case 3 : {
                $this->content = "<div id='sous'><h1>Voici le contenu du cadeau et le montant déjà enregistré à la cagnotte.<br> Le montant de la cagnotte est de : " . $_SESSION['montantContri'] . " €.</h1>";
                foreach ($this->objet as $p) {
                    $this->content .= "<div id='sous'><H2>" . $p['id'] . "</H2><H1>" . $p['nom'] . "<H1>" . $p['descr'] . "</H1><img src=\"$this->appRoot/web/img/" . $p['img'] . "\" alt=\"" . $p['img'] . "\">" . "</H1></div>";
                }
                $res = $this->afficherContenuCadeauCagnotte();
                break;
            }
            case 4 : {
                $this->content = "<div id='sous'><H1>Indiquez le montant que vous voulez donner.</H1>";
                $res = $this->afficherParticiperCagnotte();
                break;
            }
            case 5 : {
                $this->content = "<div id='sous'><H1>Votre contribution a bien été enregistrée.</H1>";
                $res = $this->afficherParticipationEnreg();
                break;
            }
            case 6 : {
                if (!isset($_SESSION['montantContri']))
                    $_SESSION['montantContri'] = 0;
                $this->content = "<div id='sous'><H1>Voici le montant déjà enregistré à votre cagnotte : " . $_SESSION['montantContri'] . " €.</H1>";
                $res = $this->afficherGestionCagnotte();
                break;
            }
            case 7 : {
                $this->content = "<div id='sous'><H1>Le montant de votre cagnotte n'est pas complet. Votre cagnotte n'a pas pu être cloturer.</H1>";
                $res = $this->afficherCagnotteNonCloturer();
                break;
            }
            case 8 : {
                $this->content = "<div id='sous'><H1>Votre panier a bien été validé.</H1>";
                $res = $this->afficherPanierValide();
                break;
            }
            case 9 : {
                $this->content = "<div id='sous'><H1>La cagnotte a été cloturée, vous ne pouvez plus y contribuer.</H1>";
                $res = $this->afficherContriBloque();
                break;
            }
        }
        return $res;
    }

    private function afficherDemandeMail()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            <form method="post" action="../panier/cagnotteEnvoyee">
                $this->content
                <p><input type="submit" value="Envoyer"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherUrlCagnotteEnvoye()
    {
        $urlGestionCagnotte = $_SESSION['urlGestionCagnotte'];
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
            <title>Prestations</title> </head>
            <body>
            $this->content
            <br><a href="../cagnotteGestion?u=$urlGestionCagnotte">
                <p><input type="submit" value="Gestion de la cagnotte"></p>
            </a>
            <form action="../listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherParticipationEnreg()
    {
        $urlCagnotte = $_SESSION['urlCagnotte'];
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <a href="../cagnotte?u=$urlCagnotte">
                <p><input type="submit" value="Retour à la cagnotte"></p>
            </a>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherContenuCadeauCagnotte()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Contenu cadeau</title> </head>
            <body>
            $this->content
            <form action="./cagnotte?" method="get">
                <p><input type="submit" name="p" value="Participer"></p>
            </form>
            <form action="./listeprestation">
                <p><input type="submit" value="Accéder au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherParticiperCagnotte()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Contenu cadeau</title> </head>
            <body>
            $this->content
            <form action="./cagnotte/participe" method="post">
                <h1><a><p><b><input type="number" id="montant" name="montant" maxlength="4" placeholder="Ex : 20" required autofocus></b></a></h1></p>
            
                <p><input type="submit" value="Valider"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherGestionCagnotte()
    {
        if ($_SESSION['montantContri'] >= $_SESSION['montantPanier'])
            $url = "./cloturerCagnotte?u=" . $_SESSION['urlGestionCagnotte'] . "&ca=valide";
        else
            $url = "./cloturerCagnotte?u=" . $_SESSION['urlGestionCagnotte'] . "&ca=nonValide";
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Gestion Cagnotte</title> </head>
            <body>
            $this->content
            <a href="$url">
                <p><input type="submit" value="Cloturer la cagnotte"></p>
            </a>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherCagnotteNonCloturer()
    {
        $urlGestionCagnotte = $_SESSION['urlGestionCagnotte'];
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <a href="./cagnotteGestion?u=$urlGestionCagnotte">
                <p><input type="submit" value="Retour à la cagnotte"></p>
            </a>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPanierValide()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form action="./cadeau">
                <p><input type="submit" value="Générer URL cadeau"></p>
            </form>
            <form action="./listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherContriBloque()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form action="./listeprestation">
                <p><input type="submit" value="Accéder au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

//genere l'entete du site à inclure sur l'html de toutes les pages!!!!!!!!!
    public function menu()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="web/CSS/bootstrap.css" rel="stylesheet">
    <link href="web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="./">Accueil</a></li>
                <li><a href="./panier">Panier</a></li>
                <li><a href="./listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="./listecategorie">Categories</a></li>
                <li>  <a href="./connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }

    // même menu que celui au-dessus mais adapté pour qu'il fonctionne avec la liste des prestations d'une catégorie
    public function menu2()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="../web/CSS/bootstrap.css" rel="stylesheet">
    <link href="../web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="../">Accueil</a></li>
                <li><a href="../panier">Panier</a></li>
                <li><a href="../listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="../listecategorie">Categories</a></li>
                <li>  <a href="../connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }
}