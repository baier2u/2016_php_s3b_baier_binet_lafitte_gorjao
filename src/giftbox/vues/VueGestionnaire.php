<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 03/01/2017
 * Time: 17:22
 */

namespace giftbox\vues;


class VueGestionnaire
{
    public $content, $objet;

    function __construct($tab)
    {
        $this->objet = $tab;
    }

    public function render($num)
    {
        switch ($num) {
            case 1 : {
                return ($this->afficherFormulaire());
                break;
            }
            case 2 : {
                $this->content = "<div id='sous'><H1>Votre inscription a été bien été validé.</H1>";
                return ($this->afficherValidationInscription());
                break;
            }
            case 3 : {
                return ($this->afficherConnexion());
                break;
            }
            case 4 : {
                return ($this->afficherPageGestion());
                break;
            }
            case 5 : {
                $this->content = "<div id='sous'><H1>Votre identifiant ou votre mot de passe est invalide.</H1>";
                return ($this->afficherConnexionInvalide());
                break;
            }
            case 6 : {
                return ($this->afficherSuppressionPrestation());
                break;
            }
            case 7 : {
                $this->content = "<div id='sous'><H1>$this->objet</H1>";
                return ($this->afficherPrestationSupprimee());
                break;
            }
            case 8 : {
                return ($this->afficherAjoutPrestation());
                break;
            }
            case 9 : {
                $this->content = "<div id='sous'><H1>$this->objet</H1>";
                return ($this->afficherPrestationAjoutee());
                break;
            }
            case 10 : {
                return ($this->afficherDesactivationPrestation());
                break;
            }
            case 11 : {
                $this->content = "<div id='sous'><H1>$this->objet</H1>";
                return ($this->afficherPrestationDesactivee());
                break;
            }
            case 12 : {
                $this->content = "<div id='sous'><H1>$this->objet</H1>";
                return ($this->afficherDeconnexion());
                break;
            }
        }
        return null;
    }

    private function afficherFormulaire()
    {
        $html = $this->menuSansPanier();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            <form method="post" action="./validation">
                <div id='sous'><h1><a><p><b>Pour vous inscrire vous devez remplir ce formulaire.<br>
                
                <h1><a><p><b><label for="nom">Entrez votre identifiant</label> : <input type="texte" name="identifiant"
                maxlength="40" placeholder="Ex : Dupont54" autofocus required></b></a></h1></p>
                
                <h1><a><p><b><label for="passwd">Entrez votre mot de passe</label> : <input type="password" name="passwd"
                maxlength="40" required></b></a></h1></p>
                
                <p><input type="submit" value="Valider"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherValidationInscription()
    {
        $html = $this->menuSansPanier();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form action="./connexion">
                <p><input type="submit" value="Connexion"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherConnexion()
    {
        $html = $this->menuSansPanier();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            <form method="post" action="./gestionnaire">
                <div id='sous'><h1><a><p><b>Veuillez entrer votre identifiant et votre mot de passe pour vous connecter.<br>
                
                <h1><a><p><b><input type="texte" name="identifiant"
                maxlength="40" placeholder="Ex : Dupont54" autofocus required></b></a></h1></p>
                
                <h1><a><p><b><input type="password" name="passwd"
                maxlength="40" required></b></a></h1></p>
                
                <p><input type="submit" value="Valider"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPageGestion()
    {
        if (isset($_SESSION['connexion'])) {
            $urlAjouter = "./ajoutPrestation";
            $urlSupprimer = "./suppressionPrestation";
            $urlDesactiver = "./desactivationPrestation";
        } else {
            $urlAjouter = "./connexion";
            $urlSupprimer = "./connexion";
            $urlDesactiver = "./connexion";
        }
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            <div id='sous'>
             <form action="$urlAjouter">
                <p><input type="submit" value="Ajouter une prestation"></p>
            </form>
            <form action="$urlSupprimer">
                <p><input type="submit" value="Supprimer une prestation"></p>
            </form>
            <form action="$urlDesactiver">
                <p><input type="submit" value="Désactiver une prestation"></p>
            </form>
            <form action="./listeprestation">
                <p><input type="submit" value="Accéder au catalogue"></p>
            </form>
            <form action="./deconnexion">
                <br><p><input type="submit" value="Se déconnecter"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherConnexionInvalide()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form method="post" action="./connexion">
                <p><input type="submit" value="Connexion"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherSuppressionPrestation()
    {
        $html = $this->menuSansPanier();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head> 
            <title>Prestations</title> </head>
            <body>
            <form action="./suppressionPrestation" method="get">
                <div id='sous'><h1><p><b><label for="suppresion">Entrez l'id de la prestation à supprimer</label> : </h1><input type="texte" id="suppression" name="sup"
                maxlength="6" placeholder="Ex : 11" required autofocus></b></p>
                <p><input type="submit" value="Supprimer"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPrestationSupprimee()
    {
        if (isset($_SESSION['connexion'])) {
            $urlAjouter = "./ajoutPrestation";
            $urlSupprimer = "./suppressionPrestation";
            $urlDesactiver = "./desactivationPrestation";
        } else {
            $urlAjouter = "./connexion";
            $urlSupprimer = "./connexion";
            $urlDesactiver = "./connexion";
        }
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <div id='sous'>
             <form action="$urlAjouter">
                <p><input type="submit" value="Ajouter une prestation"></p>
            </form>
            <form action="$urlSupprimer">
                <p><input type="submit" value="Supprimer une prestation"></p>
            </form>
            <form action="$urlDesactiver">
                <p><input type="submit" value="Désactiver une prestation"></p>
            </form>
            <form action="./listeprestation">
                <p><input type="submit" value="Accéder au catalogue"></p>
            </form>
            <form action="./deconnexion">
                <br><p><input type="submit" value="Se déconnecter"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherAjoutPrestation()
    {
        $html = $this->menuSansPanier();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head> 
            <title>Prestations</title> </head>
            <body>
            <form action="./ajoutPrestation" method="post" enctype="multipart/form-data">
                <div id='sous'><a><h1><p><b><label>Remplissez ce formulaire pour ajouter une nouvelle prestation.</label></h1></a>
                
                <h1><p><b><label for="nom">Entrez le nom de la prestation</label> : </h1><input type="texte" id="nom" name="nom"
                maxlength="6" placeholder="Ex : Coca" required autofocus></b></p>
                
                <h1><p><b><label for="description">Entrez la description de la prestation</label> : 
                <textarea id="description" name="descr" required></textarea></b></p></h1>
                
                <h1><p><b><label for="categorie">Entrez l'id de la catégorie de la prestation</label> : </h1><input type="texte" id="categorie" name="cat_id"
                maxlength="6" placeholder="Ex : 2"></b></p>
                
                <h1><p><b><label for="image">Chargez l'image de la prestation</label> : </h1><input type="file" id="image" name="img" required></b></p>
                
                <h1><p><b><label for="prix">Entrez le prix de la prestation</label> : </h1><input type="texte" id="prix" name="prix"
                maxlength="6" placeholder="Ex : 10"></b></p>
                
                <p><input type="submit" value="Ajouter"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPrestationAjoutee()
    {
        if (isset($_SESSION['connexion'])) {
            $urlAjouter = "./ajoutPrestation";
            $urlSupprimer = "./suppressionPrestation";
            $urlDesactiver = "./desactivationPrestation";
        } else {
            $urlAjouter = "./connexion";
            $urlSupprimer = "./connexion";
            $urlDesactiver = "./connexion";
        }
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <div id='sous'>
             <form action="$urlAjouter">
                <p><input type="submit" value="Ajouter une prestation"></p>
            </form>
            <form action="$urlSupprimer">
                <p><input type="submit" value="Supprimer une prestation"></p>
            </form>
            <form action="$urlDesactiver">
                <p><input type="submit" value="Désactiver une prestation"></p>
            </form>
            <form action="./listeprestation">
                <p><input type="submit" value="Accéder au catalogue"></p>
            </form>
            <form action="./deconnexion">
                <br><p><input type="submit" value="Se déconnecter"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherDesactivationPrestation()
    {
        $html = $this->menuSansPanier();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head> 
            <title>Prestations</title> </head>
            <body>
            <form action="./desactivationPrestation" method="get">
                <div id='sous'><h1><p><b><label for="desactivation">Entrez l'id de la prestation à désactiver</label> : </h1><input type="texte" id="desactiver" name="des"
                maxlength="6" placeholder="Ex : 11" required autofocus></b></p>
                
                <h1><p><b><label for="temps">Entrez le nombre de jours que la prestation doit être désactivée</label> : </h1><input type="texte" id="temps" name="time"
                maxlength="6" placeholder="Ex : 3" required></b></p>
                
                <p><input type="submit" value="Désactiver"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPrestationDesactivee()
    {
        if (isset($_SESSION['connexion'])) {
            $urlAjouter = "./ajoutPrestation";
            $urlSupprimer = "./suppressionPrestation";
            $urlDesactiver = "./desactivationPrestation";
        } else {
            $urlAjouter = "./connexion";
            $urlSupprimer = "./connexion";
            $urlDesactiver = "./connexion";
        }
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <div id='sous'>
             <form action="$urlAjouter">
                <p><input type="submit" value="Ajouter une prestation"></p>
            </form>
            <form action="$urlSupprimer">
                <p><input type="submit" value="Supprimer une prestation"></p>
            </form>
            <form action="$urlDesactiver">
                <p><input type="submit" value="Désactiver une prestation"></p>
            </form>
            <form action="./listeprestation">
                <p><input type="submit" value="Accéder au catalogue"></p>
            </form>
            <form action="./deconnexion" method="get">
                <br><p><input type="submit" name="deco" value="Se déconnecter"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherDeconnexion()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form action="./listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

//genere l'entete du site à inclure sur l'html de toutes les pages!!!!!!!!!
    public function menu()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="web/CSS/bootstrap.css" rel="stylesheet">
    <link href="web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="./">Accueil</a></li>
                <li><a href="./panier">Panier</a></li>
                <li><a href="./listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="./listecategorie">Categories</a></li>
                <li>  <a href="./connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }

    // même menu que celui au-dessus mais adapté pour qu'il fonctionne avec la liste des prestations d'une catégorie
    public function menu2()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="../web/CSS/bootstrap.css" rel="stylesheet">
    <link href="../web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="../">Accueil</a></li>
                <li><a href="../panier">Panier</a></li>
                <li><a href="../listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="../listecategorie">Categories</a></li>
                <li>  <a href="../connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }

    // génère l'entete du site sans le panier
    public function menuSansPanier()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="web/CSS/bootstrap.css" rel="stylesheet">
    <link href="web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="./">Accueil</a></li>
                <li><a href="./listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="./listecategorie">Categories</a></li>
                <li>  <a href="./connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }
}