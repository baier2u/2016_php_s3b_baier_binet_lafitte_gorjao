<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 19/12/2016
 * Time: 18:01
 */

namespace giftbox\vues;


class VueCoffret
{
    public $objet, $content, $nom, $prenom;

    function __construct($tab)
    {
        $this->objet = $tab;
    }

    public function render($num)
    {
        $res = null;
        switch ($num) {
            // cas si la variable $_SESSION['panier'] existe et contient des valeurs
            case 1 : {
                $this->content = "<div id='sous'><H1>Voici le contenu de votre panier :<br></H1></div>";
                $montant = 0;
                foreach ($this->objet as $p) {
                    $montant += $p['prix'];
                    $this->content .= "<div id='sous'><H2>" . $p['id'] . "</H2>  <H1>" . $p['nom'] . "</H1><br><H1>" . $p['prix'] . " €</H1><br></div>";
                }
                $_SESSION['montantPanier'] = $montant;
                $this->content .= "<div id='sous'><H1>Le montant total du panier est de :<br>" . $montant . " €</H1><br></div>";
                // va afficher l'id, le nom et le prix des prestations du panier
                $res = $this->afficherListePrestation();
                break;
            }
            // cas si la variable $_SESSION['panier'] n'existe pas
            case 2 : {
                $this->content = "<div id='sous'><H1>Votre panier est vide.</H1>";
                $res = $this->afficherPanierVide();
                break;
            }
            case 3 : {
                // le panier n'est pas validé et donc on affiche son contenu et un message
                $this->content = "<div id='sous'><H1>Votre panier n'a pas été validé, pour l'être il doit contenir deux prestations différentes et de deux catégories différentes.<br></H1></div>";
                $montant = 0;
                foreach ($this->objet as $p) {
                    $montant += $p['prix'];
                    $this->content .= "<div id='sous'><H2>" . $p['id'] . "</H2>  <H1>" . $p['nom'] . "</H1><br><H1>" . $p['prix'] . " €</H1><br></div>";
                }
                $this->content .= "<div id='sous'><H1>Le montant total du panier est de :<br>" . $montant . " €</H1><br></div>";
                // va afficher le contenu du panier
                $res = $this->afficherPanierNonValide();
                break;
            }
            case 4 : {
                // la panier est validé et vidé mais il n'est pas stocké dans la bdd car il n'y a pas de valeurs dans post..
                $this->content = "<div id='sous'><H1>Votre panier a bien été validé.</H1>";
                $res = $this->afficherPanierValide();
                break;
            }
            case 5 : {
                // va afficher une page qui demande a l'utilisateur de rentrer ses informations
                $res = $this->afficherPanierInformations();
                break;
            }
            case 6 : {
                // va afficher une page qui demande a l'utilisateur de rentrer ses informations de paiement pour le paiement classique
                $res = $this->demanderPaiement();
                break;
            }
            case 7 : {
                $this->content = "<div id='sous'><H1><a>Une url cadeau a été généré, à qui voulez-vous l'envoyez ?</a></H1><br>";
                $res = $this->afficherURLCadeau();
                break;
            }
            case 8 : {
                $res = $this->demanderNbContributeur();
                break;
            }
            case 9 : {
                $this->content = "<div id='sous'><H1>Voici le récapitulatif de votre panier.<br></H1></div>";
                $montant = 0;
                foreach ($this->objet as $p) {
                    $montant += $p['prix'];
                    $this->content .= "<div id='sous'><H2>" . $p['id'] . "</H2>  <H1>" . $p['nom'] . "</H1><br><H1>" . $p['prix'] . " €</H1><br></div>";
                }
                $this->content .= "<div id='sous'><H1>Le montant total du panier est de :<br>" . $montant . " €</H1><br>";
                $res = $this->afficherPanierRecapitulatif();
                break;
            }
        }
        return $res;
    }

    private function afficherListePrestation()
    {
        // l'action du bouton valider est différente selon si le panier peut être validé ou non
        $boutonValider = null;
        if ($_SESSION['nbPrest'] <= 2 && $_SESSION['nbCate'] < 2) {
            $boutonValider = '<form action="./panier/nonValide">
                                    <input type="submit" value="Valider le panier">
                               </form>';
        } else {
            $boutonValider = '<form action="./panier/infos">
                                    <input type="submit" value="Valider le panier">
                               </form>';
        }
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <div id="sous">
            <br>
            $boutonValider
            <br>
            <form action="./panier" method="get">
                <p><b><label for="suppresion">Entrez l'id de la prestation à supprimer</label> : <input type="texte" id="suppression" name="sup"
                maxlength="6" placeholder="Ex : 11"></b></p>
                <p><input type="submit" value="Supprimer"><br><br></p>
            </form>
            <form action="./panier" method="get">
                <p><input type="submit" name="vider" value="Vider le panier"></p>
            </form>
            <form action="./listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPanierVide()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form action="./listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPanierNonValide()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
          
            $this->content
            <div class ="sous">
            <form action="../panier" method="get">
                <p><b><label for="suppresion">Entrez l'id de la prestation à supprimer</label> : <input type="texte" id="suppression" name="sup"
                maxlength="6" placeholder="Ex : 11"></b></p>
                <p><input type="submit" value="Supprimer"></p>
            </form>
            <form action="../panier" method="get">
                <p><input type="submit" name="vider" value="Vider le panier"></p>
            </form>
            <form action="../listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPanierValide()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form action="../cadeau">
                <p><input type="submit" value="Générer URL cadeau"></p>
            </form>
            <form action="../listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherPanierRecapitulatif()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form method="post" action="../panier/paye">
                <p><input type="submit" value="Valider"></p>
            </form>
            </div>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    // méthode qui permet d'afficher la page où l'utilisateur doit rentrer ses infos
    private function afficherPanierInformations()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            <form method="post" action="../panier/valide">
                <div id='sous'><h1><a><p><b><label for="name">Entrez votre nom</label> : <input type="texte" id="name" name="nom"
                maxlength="20" placeholder="Ex : Dupont" autofocus required></b></a></h1></p><br>
                
                <h1><a><p><b><label for="username">Entrez votre prénom</label> : <input type="texte" id="username" name="prenom"
                maxlength="20" placeholder="Ex : Jean" required></b></a></h1></p><br>
                
                <h1><a><p><b><label for="mail">Entrez votre adresse mail</label> : <input type="email" id="mail" name="mailUtil"
                maxlength="40" placeholder="Ex : dupontjean@gmail.com" required></b></a></h1></p><br>
                
                <h1><a><p><b><label for="mess">Entrez votre message</label> : <textarea id="mess" name="message" required></textarea></b></a></h1></p>
               
                <h1><a><p><b><label for="paiementClassique">Choisissez votre paiement.</label><br><select id="paiementClassique" name="paiement">
                    <option>Classique</option>
                    <option>Cagnotte</option>
                </select></b></a></h1></p>
                
                <p><input type="submit" value="Valider"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function demanderPaiement()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Paiement</title> </head>
            <body>
            <form method="post" action="../panier/recapitulatif">
                <div id='sous'><h1><a><p><b><label for="name">Entrez votre nom</label> : <input type="texte" id="name" name="nom"
                maxlength="20" placeholder="Ex : Dupont" autofocus required></b></a></h1></p><br>
                
                <h1><a><p><b><label for="username">Entrez votre prénom</label> : <input type="texte" id="username" name="prenom"
                maxlength="20" placeholder="Ex : Jean" required></b></a></h1></p><br>
                
                
                <h1><a><p><b><label for="numeroCarte">Entrez votre numéro de carte</label> : <input type="texte" id="numeroCarte" name="carte"
                maxlength="20" placeholder="Ex : 4852 9522 6584 5482" required></b></a></h1></p><br>
                
                <h1><a><p><b><label for="cryptogramme">Entrez votre cryptogramme</label> : <input type="texte" id="cryptogramme" name="crypto"
                maxlength="3" placeholder="Ex : 065" required></b></a></h1></p><br>
                
                
                <h1><a><p><b><label for="paiementClassique">J'accèpte le règlement</label><input type="checkbox" id="paiementClassique" name="paye" value="paye" required>
                </b></a></h1></p>
                
                <p><input type="submit" value="Valider"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function afficherURLCadeau() {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <h1><a><b><label for="mail">Entrez l'adresse mail du destinataire</label> : <input type="email" name="mail"
            maxlength="20" placeholder="Ex : dupontjean@gmail.com" autofocus></b></a></h1>
            <form action="./listeprestation">
                <p><input type="submit" value="Envoyer"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    private function demanderNbContributeur()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            <form method="post" action="../panier/cagnotteContri">
                <div id='sous'><h1><a><p><b><label for="nbpers">Combien de personnes doivent contribuer à votre cagnotte ?</label><input type="number" id="nbpers" name="nbpers"
                maxlength="4" placeholder="Ex : 2" required autofocus></b></a></h1></p>
              
                <p><input type="submit" value="Valider"></p></div>
            </form>
            </body>
            </html>
END;
        return $html;
    }

//genere l'entete du site à inclure sur l'html de toutes les pages!!!!!!!!!
    public function menu()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="web/CSS/bootstrap.css" rel="stylesheet">
    <link href="web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="./">Accueil</a></li>
                <li><a href="./panier">Panier</a></li>
                <li><a href="./listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="./listecategorie">Categories</a></li>
                <li>  <a href="./connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }

    // même menu que celui au-dessus mais adapté pour qu'il fonctionne avec la liste des prestations d'une catégorie
    public function menu2()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="../web/CSS/bootstrap.css" rel="stylesheet">
    <link href="../web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="../">Accueil</a></li>
                <li><a href="../panier">Panier</a></li>
                <li><a href="../listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="../listecategorie">Categories</a></li>
                <li>  <a href="../connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }
}