<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 29/12/2016
 * Time: 15:38
 */

namespace giftbox\vues;


class VueCadeau
{
    public $objet, $content, $appRoot;

    function __construct($tab)
    {
        $this->objet = $tab;
        $this->appRoot = \Slim\Slim::getInstance()->request->getRootUri();
    }

    public function render($num)
    {
        $res = null;
        switch ($num) {
            case 1 : {
                $this->content = "<div id='sous'><H1><a>Une url cadeau a été générée, à qui voulez-vous l'envoyer ?</a></H1><br>";
                $res = $this->afficherURLCadeau();
                break;
            }
            case 2 : {
                $this->content = "<div id='sous'><H1><a>Voici le contenu du cadeau qu'on vous a envoyé :</a></H1><br>";
                foreach ($this->objet as $p) {
                    $this->content .= "<br><div id='sous'><H1>" . $p['nom'] . "<H1><br>" . $p['descr'] . "</H1>" . "<img src=\"$this->appRoot/web/img/" . $p['img'] . "\" alt=\"" . $p['img'] . "\">" . "</H1></div>";
                }
                $res = $this->afficherContenuCadeau('t');
                break;
            }
            case 3 : {
                $this->content = "<div id='sous'><H1><a>Voici le contenu du cadeau qu'on vous a envoyé :</a></H1><br>";
                $p = $this->objet;
                $this->content .= "<br><div id='sous'><H1>" . $p['nom'] . "<H1><br>" . $p['descr'] . "</H1>" . "<img src=\"$this->appRoot/web/img/" . $p['img'] . "\" alt=\"" . $p['img'] . "\">" . "</H1></div>";
                $res = $this->afficherContenuCadeau('i');
                break;
            }
            case 4 : {
                $this->content = "<div id='sous'><H1>Votre mail a bien été envoyé.</H1>";
                $res = $this->cadeauEnvoye();
                break;
            }
            case 5 : {
                $this->content = "<div id='sous'><H1>Vous ne pouvez pas accéder à votre cadeau pour l'instant.</H1>";
                $res = $this->afficherContenuCadeau('t');
                break;
            }
            case 6 : {
                $this->content = "<div id='sous'><H1>Le cadeau que vous avez envoyé est " . $this->objet['etatCadeau'] . ".</H1>";
                $res = $this->afficherGestionCadeau();
                break;
            }
            case 7 : {
                $this->content = "<div id='sous'><H1>Entrez le mot de passe du coffret.</H1>";
                $res = $this->afficherMDPCoffret();
                break;
            }
        }
        return $res;
    }

    // affiche la page qui génère l'url à envoyer et demande à l'utilisateur d'entre le mail du destinataire
    private function afficherURLCadeau()
    {
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <form method="post" action="./cadeau/envoye">
                <h1><a><b><label for="mdp">Entrez le mot de passe pour votre coffret : </label><input type="passwd" name="mdp"
                maxlength="40" required autofocus></b></a></h1>
            
                <h1><a><b><label for="mailDestinataire">Entrez l'adresse mail du destinataire</label> : <input type="email" name="mailDestinataire"
                maxlength="40" placeholder="Ex : dupontjean@gmail.com" required></b></a></h1>
                
                <h1><a><p><b><label for="affichage">Choisissez l'affichage pour votre cadeau.</label><br><select id="AffichageCadeau" name="affichage" required>
                    <option>Tout afficher à une date</option>
                    <option>Individuel</option>
                </select></b></a></h1></p>

                <h1><a><p><b><label for="dateCadeau">A partir de quand pourra-t-on l'ouvrir ?</label><br>
                
                <input type="date" name="dateCadeau" required>
                </b></a></h1></p>
           
                <p><input type="submit" value="Envoyer"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    // affiche le contenu du cadeau qui a été envoyé
    private function afficherContenuCadeau($affiche)
    {
        $lienPagePrec = null;
        $lienPageSuiv = null;
        $url = $_SESSION['url'];
        if ($affiche == 'i') {
            if ($_SESSION['iteration'] == 0 && sizeof($_SESSION['contenuCadeau']) > 0) {
                $lienPageSuiv = "<a href=\"./offrecadeau?u=$url&page=suiv\">Page suivante</a><br>";
            } else if ($_SESSION['iteration'] == (sizeof($_SESSION['contenuCadeau']) - 1) && sizeof($_SESSION['contenuCadeau']) > 0) {
                $lienPagePrec = "<a href=\"./offrecadeau?u=$url&page=prec\">Page précédente</a><br>";
            } else if (sizeof($_SESSION['contenuCadeau']) > 0) {
                $lienPagePrec = "<a href=\"./offrecadeau?u=$url&page=prec\">Page précédente</a>";
                $lienPageSuiv = "<a href=\"./offrecadeau?u=$url&page=suiv\">Page suivante</a><br>";
            }
        }
        $html = $this->menu();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            $lienPagePrec
            $lienPageSuiv
            <form action="./listeprestation">
                <p><input type="submit" value="Accéder au catalogue"></p>
            </form>
            </div>
            </body>
            </html>
END;
        return $html;
    }

    // affiche la page qui dit que le mail a bien été envoyé mais pour l'instant la fonction mail ne marche pas
    private function cadeauEnvoye()
    {
        $urlGestion = $_SESSION['urlGestion'];
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Prestations</title> </head>
            <body>
            $this->content
            <h1><a href="../gestionCadeau/$urlGestion">Accéder à la gestion du cadeau</a></h1>
            <form action="../listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    public function afficherGestionCadeau()
    {
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Gestion du cadeau</title> </head>
            <body>
            $this->content
            <form action="../listeprestation">
                <p><input type="submit" value="Retour au catalogue"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

    public function afficherMDPCoffret()
    {
        $urlGestion = $_SESSION['urlGestion'];
        $html = $this->menu2();
        $html .= <<<END
            <!DOCTYPE html>
            <html>
            <head>
             <title>Gestion du cadeau</title> </head>
            <body>
            $this->content
            <form method="post" action="./$urlGestion">
                <h1><a><b><label for="mdp">Entrez le mot de passe de votre coffret</label> : <input type="passwd" name="mdp"
                maxlength="40" required autofocus></b></a></h1>
                
                <p><input type="submit" value="Valider"></p>
            </form>
            </body>
            </html>
END;
        return $html;
    }

//genere l'entete du site à inclure sur l'html de toutes les pages!!!!!!!!!
    public function menu()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="web/CSS/bootstrap.css" rel="stylesheet">
    <link href="web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="./">Accueil</a></li>
                <li><a href="./panier">Panier</a></li>
                <li><a href="./listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="./listecategorie">Categories</a></li>
                <li>  <a href="./connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }

    // même menu que celui au-dessus mais adapté pour qu'il fonctionne avec la liste des prestations d'une catégorie
    public function menu2()
    {
        $html = <<<END
     <!DOCTYPE html>
<html>
<head>
    <link href="../web/CSS/bootstrap.css" rel="stylesheet">
    <link href="../web/CSS/starter-template.css" rel="stylesheet">
</head>
<body>



    <div class="container-full">

        <div class="navbar navbar-default menu">
            <div class="navbar-header ">
                <a class="navbar-brand" href="#">Giftbox</a>
            </div>
            <ul class="nav navbar-nav ">
                <li><a href="../">Accueil</a></li>
                <li><a href="../panier">Panier</a></li>
                <li><a href="../listeprestation">Prestations</a></li>
                <li class="divider-vertical"></li>
                <li><a href="../listecategorie">Categories</a></li>
                <li>  <a href="../connexion">Connexion</a></li>
            </ul>
        </div>
    </div>
</body>

</html>
END;
        return $html;
    }
}