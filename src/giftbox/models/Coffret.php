<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 27/12/2016
 * Time: 16:09
 */

namespace giftbox\models;


class Coffret extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'coffret';
    protected $primaryKey = 'id';
    public $timestamps = false;
}