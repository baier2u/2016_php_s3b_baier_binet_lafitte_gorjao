<?php

/**
 * Created by PhpStorm.
 * User: baier
 * Date: 05/12/2016
 * Time: 11:04
 */
namespace giftbox\models;

class Categorie extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;

    function listerPrestations() {
        return $this->hasMany('\giftbox\models\Prestation', 'cat_id');
    }
}