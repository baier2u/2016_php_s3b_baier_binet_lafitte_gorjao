<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 14/01/2017
 * Time: 13:12
 */

namespace giftbox\models;


class Gestionnaire extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'gestionnaire';
    protected $primaryKey = 'identifiant';
    public $timestamps = false;
}