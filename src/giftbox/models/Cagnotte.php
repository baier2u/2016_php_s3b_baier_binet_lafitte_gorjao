<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 12/01/2017
 * Time: 21:27
 */

namespace giftbox\models;


class Cagnotte extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'cagnotte';
    protected $primaryKey = 'id';
    public $timestamps = false;
}