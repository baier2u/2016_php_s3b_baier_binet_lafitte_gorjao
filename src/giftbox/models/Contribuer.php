<?php
/**
 * Created by PhpStorm.
 * User: baier
 * Date: 12/01/2017
 * Time: 23:05
 */

namespace giftbox\models;


class Contribuer extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'contribuer';
    protected $primaryKey = 'id_coffret';
    public $timestamps = false;
}