<?php

/**
 * Created by PhpStorm.
 * User: baier
 * Date: 05/12/2016
 * Time: 11:03
 */
namespace giftbox\models;

class Prestation extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'prestation';
    protected $primaryKey = 'id';
    public $timestamps = false;

    function donnerCate() {
        return $this->belongsTo('\giftbox\models\Categorie', 'cat_id');
    }
}