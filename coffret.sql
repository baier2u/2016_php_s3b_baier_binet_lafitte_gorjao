﻿
CREATE TABLE coffret (
id INTEGER not NULL AUTO_INCREMENT,
nom VARCHAR (40) not null,
prenom VARCHAR (40) not null,
mail VARCHAR (40) not null,
message VARCHAR (40) not null,
montant INTEGER not null,
paiement VARCHAR (40) not null,
etat VARCHAR (40) not null,
contenu TEXT not null,
urlGestion VARCHAR(100) not null,
mdpGestion VARCHAR(300) not null,
urlCadeau VARCHAR(100) not null,
etatCadeau VARCHAR(50) not null,
dateCadeau DATE not null,
PRIMARY KEY (id)
);